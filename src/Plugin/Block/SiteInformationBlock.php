<?php

namespace Drupal\siteinfo\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\siteinfo\Controller\SiteInformationController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'FacebookPostBlock' block.
 *
 * @Block(
 *  id = "site_information",
 *  admin_label = @Translation("Site Information"),
 * )
 */

class SiteInformationBlock extends BlockBase implements ContainerFactoryPluginInterface, BlockPluginInterface {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $siteinfo = SiteInformationController::siteInformation();
    $content = $this->renderer->render($siteinfo);
    return [
      '#type' => 'markup',
      '#markup' => $content,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access site information');
  }

}
