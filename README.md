INTRODUCTION
------------

Site Info is a module which display site related information like: about the site,
drupal, php, database, etc. This module extends Drupal to provide brief
information about the website. That information helpful to site admin. Site admin
got site information in one page.

INSTALLATION
------------

The siteinfo.module is very similar to other Drupal modules. For installation of
the siteinfo module please follow the below mentioned steps:

• Install as usual
• Enable the siteinfo module.

CONFIGURATION
-------------

• There is no configuration for this module.

MAINTAINERS
-----------

Current Maintainers:

• Rahul Seth          - https://www.drupal.org/user/2694359
• Rishi Kulshreshtha  - https://www.drupal.org/user/1403808
